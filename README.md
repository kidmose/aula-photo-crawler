Python script for logging in to, and downloading all available images
from Aula (LMS available at https://www.aula.dk).

Enter credentials into `aula.ini`;

    [credentials]
    username = ...
    password = ...

Run with `nix-shell --pure --run "python aula.py"` if you have
`nix`. Check `default.nix` for dependencies otherwise.
