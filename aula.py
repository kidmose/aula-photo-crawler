from pathlib import Path
from tempfile import TemporaryFile
import json
import os
import requests
import time
import configparser

from bs4 import BeautifulSoup
import dateutil.parser

cfg = configparser.ConfigParser()
cfg.read("aula.ini")


def submit_login_form(response, session, credentials):
    """
    Takes a response, finds login form, and submits it, to get the next response.
    """

    if response.status_code != 200:
        raise Exception(f"Response status code is not 200, but: {response.status_code}")

    soup = BeautifulSoup(response.text, "html.parser")

    form_data = {}
    inputs = soup.find_all("input")
    for i in inputs:
        if i.has_attr("name"):
            if i["name"] in ("username", "password",):
                form_data[i["name"]] = credentials[i["name"]]
            elif i.has_attr("value"):
                form_data[i["name"]] = i["value"]

    url = soup.form["action"]

    return session.post(url, data=form_data)


# log in
session = requests.Session()
response = session.get("https://login.aula.dk/auth/login.php?type=unilogin")
for i in range(10):
    response = submit_login_form(response, session, cfg["credentials"])
    if response.url == "https://www.aula.dk:443/portal/":
        print("Logged in")
        break

# What kids do I have where?
# ... and without this call some method(s?) wont work, e.g. method=gallery.getAlbums
r = session.get("https://www.aula.dk/api/v15/?method=profiles.getProfileContext")
if not r.status_code == 200:
    raise Exception('Failed to "getProfileContext"')
children_in_institutions = [
    (c, i,) for i in r.json()["data"]["institutions"] for c in i["children"]
]


def albums(child, institution):
    """ Iterator of albums for child"""
    global session
    index = 0
    chunksize = 12

    def url(institution, index, chunksize):
        return (
            "https://www.aula.dk/api/v15/?method=gallery.getAlbums"
            f"&index={index}&limit={index+chunksize}"
            "&sortOn=mediaCreatedAt&orderDirection=desc&filterBy=all"
            # f"&filterInstProfileIds[]={child['id']}"
            f"&filterInstProfileIds[]={institution['institutionProfileId']}"
        )

    r = session.get(url(institution, index, chunksize))
    if r.status_code != 200:
        raise Exception("Failed to get first chunk of gallery")

    num_albums = len(r.json()['data'])

    while not index + chunksize > num_albums:  # Until we asked for more than available
        for album in r.json()["data"]:
            index += 1
            yield album

        # get next chunk
        r = session.get(url(institution, index, chunksize))


    print(f"Got {index} albums total")


def medias(aid):
    """ Iterator for media in album with id `aid` """
    global session
    index = 0
    chunksize = 10

    def url(aid, index, chunksize):
        return (
            "https://www.aula.dk/api/v15/?method=gallery.getMedia"
            f"&albumId={aid}&index={index}&limit={index+chunksize}"
        )

    r = session.get(url(aid, index, chunksize))
    if r.status_code != 200:
        raise Exception(f"Failed to get first chunk of album (id={aid})")

    title = r.json()["data"]['album']['title']
    print(f'Getting album: "{title}"')

    while index < r.json()["data"]["mediaCount"]:  # until we have them all
        # yield each media entry
        for media in r.json()["data"]["results"]:
            index += 1
            yield media

        # get next chunk
        r = session.get(url(aid, index, chunksize))

    print(f"Got {index}/{r.json()['data']['mediaCount']} from album {title}")


# TODO: Filtering images by child does not seem to work on Aula, so
# only look at the first child for now...
children_in_institutions = [children_in_institutions[0]]

for child, institution in children_in_institutions:
    print(f"{child['name']} is in {institution['name']}")

    for album in albums(child, institution):
        atitle = album["title"]
        adesc = album["description"]
        aid = album["id"]
        adate = dateutil.parser.parse(album["creationDate"])

        # Skip the pseudo album that is a view based on
        # child's tags ("Medier af dig og dine børn")
        if not aid:
            continue

        print(
            f"{child['name']} has album {album['title']}"
            f" from {institution['name']} with id={aid}"
        )

        skips = 0
        news = 0
        for media in medias(aid):
            mdate = dateutil.parser.parse(media["file"]["created"])
            fn = Path(
                "out",
                mdate.strftime("%Y"),
                mdate.strftime("%m"),
                media["file"]["name"],
            )
            fn.parent.mkdir(parents=True, exist_ok=True)

            # Skip this file if it already exists
            if fn.exists():
                skips += 1
                continue

            # Download to temporary file first, so transmission errors don't pollute
            # destination.
            with TemporaryFile() as tmp:
                r = session.get(media["file"]["url"], stream=True)
                if r.status_code != 200:
                    raise Exception(
                        f"Bad status code ({r.status_code}) with content: {r.text}"
                    )
                for mchunk in r:
                    tmp.write(mchunk)

                # From temporary file to destination
                tmp.seek(0)
                with open(fn, 'wb') as f:
                    while chunk := tmp.read(1024):
                        f.write(chunk)

                # Set file modification time
                os.utime(fn, (
                    time.mktime(time.localtime()),
                    time.mktime(mdate.timetuple()),
                ))

                # save metadata
                meta = {
                    'child': child,
                    'institution': institution,
                    'album': album,
                    'media': media,
                }
                with open(str(fn) + '.meta.json', 'w') as f:
                    json.dump(meta, f)

                news += 1

        print(
            f"Got {news} new, and skipped {skips}"
            f" for a total of {news + skips} media items,"
        )
